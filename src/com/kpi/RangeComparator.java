package com.kpi;

import java.util.Comparator;

/**
 * Created by Yuriy on 08.05.2016.
 */
public class RangeComparator implements Comparator<Planes> {
    public int compare(Planes a, Planes b){
        if (a.GetRange() > b.GetRange())
            return 1;
        else if (a.GetRange() < b.GetRange())
            return -1;
        else
            return 0;
    }
}
