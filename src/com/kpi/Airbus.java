package com.kpi;

/**
 * Created by Yuriy on 08.05.2016.
 */
public class Airbus extends Planes {
    public Airbus(){
        super();
        SetPlaneName("Airbus");
    }
    public Airbus(int r, double f, int c){
        super(r,f,c);
        SetPlaneName("Airbus");
    }
}
