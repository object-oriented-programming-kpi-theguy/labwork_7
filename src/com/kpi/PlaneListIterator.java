package com.kpi;

import java.util.ListIterator;

/**
 * Created by Yuriy on 17.06.2016.
 */
public class PlaneListIterator implements ListIterator<Planes> {

    private Planes[] airport;
    private int count;
    private int point;

    public PlaneListIterator(Planes[] air, int counter){
        airport = air;
        count = counter;
        point = 0;
    }

    @Override
    public boolean hasNext() {
        if (point >= count)
            return false;
        return true;
    }

    @Override
    public Planes next() {
        if (hasNext())
            return airport[point++];
        return null;
    }

    @Override
    public boolean hasPrevious() {
        if (point < count)
            return false;
        return true;
    }

    @Override
    public Planes previous() {
        if (hasPrevious())
            return airport[point--];
        return null;
    }

    @Override
    public int nextIndex() {
        if (hasNext())
            return point++;
        return 0;
    }

    @Override
    public int previousIndex() {
        if (hasPrevious())
            return point--;
        return 0;
    }

    @Override
    public void remove() {
        if (point < count)
            System.arraycopy(airport, point + 1, airport, point, count - point - 1);
        count--;
    }

    @Override
    public void set(Planes planes) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(Planes planes) {
        airport[point] = planes;
    }
}
