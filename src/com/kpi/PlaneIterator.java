package com.kpi;

import java.util.Iterator;

/**
 * Created by Yuriy on 17.06.2016.
 */
public class PlaneIterator implements Iterator<Planes> {
    private Planes[] airport;
    private int count;
    private int point;

    public PlaneIterator(Planes[] plan, int counter){
        airport = plan;
        count = counter;
        point = 0;
    }

    @Override
    public boolean hasNext() {
        if (point >= count)
            return false;
        return true;
    }

    @Override
    public Planes next() {
        if(hasNext())
            return airport[point++];
        return null;
    }

    @Override
    public void remove() {
        if (point < count)
            System.arraycopy(airport, point + 1, airport, point, count - point - 1);
        count--;
    }
}
