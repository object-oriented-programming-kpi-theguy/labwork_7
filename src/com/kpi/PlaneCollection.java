package com.kpi;

import java.util.*;
/**
 * Created by Yuriy on 17.06.2016.
 */
public class PlaneCollection implements List<Planes> {

    private Planes[] elements;
    private int counter;

    public PlaneCollection(){
        elements = new Planes[15];
    }

    public PlaneCollection(Planes plane){
        new PlaneCollection();
        this.add(plane);
    }

    public PlaneCollection(Collection<Planes> c){
        new PlaneCollection();
        this.addAll(c);
        addingCapacity(15);
    }

    @Override
    public int size() {
        return elements.length;
    }

    @Override
    public boolean isEmpty() {
        if(size() == 0)
            return true;
        return false;
    }

    @Override
    public boolean contains(Object o) {
        for (int i=0; i < size(); i++)
            if (o.equals(elements[i]))
                return true;
        return false;
    }

    @Override
    public Iterator<Planes> iterator() {
        return new PlaneIterator(elements, counter);
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(elements, size());
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length < size())
            return (T[]) Arrays.copyOf(elements, size(), a.getClass());
        System.arraycopy(elements, 0, a, 0, size());
        if (a.length > size())
            a[size()] = null;
        return a;
    }

    @Override
    public boolean add(Planes planes) {
        if (planes != null){
            addingCapacity(elements.length + 1);
            elements[elements.length] = planes;
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        int i = 0;
        while (o != elements[i])
            i++;
        if (i == 0)
        return false;
        System.arraycopy(elements, i+1, elements, i, 1);
        elements[size()] = null;
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        Object[] p = c.toArray();
        for (int i = 0; i < p.length; i++)
            if (contains(p[i]) == false)
        return false;
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Planes> c) {
        Object[] p = c.toArray();
        addingCapacity(this.size() + p.length);
        System.arraycopy(p,0,elements,size(),p.length);
        return p.length != 0;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Planes> c) {
        Object[] p = c.toArray();
        addingCapacity(this.size() + p.length);
        System.arraycopy(elements, index, elements, index + p.length, this.size());
        System.arraycopy(p, 0, elements, index, p.length);
        return p.length != 0;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Object[] p = c.toArray();
        for (int i = 0; i < p.length; i++){
            remove(p[i]);
            if (remove(p[i]))
                return true;
        }
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Object[] p  =c.toArray();
        for (int i = 0; i < p.length; i++){
            if (contains(p[i])){
                remove(p[i]);
                if (remove(p[i]))
                    return true;
            }
        }
        return false;
    }

    @Override
    public void clear() {
        elements = null;
    }

    @Override
    public Planes get(int index) {
        return elements[index];
    }

    @Override
    public Planes set(int index, Planes element) {
        if (elements[index] != null)
            elements[index] = element;
        return element;
    }

    @Override
    public void add(int index, Planes element) {
        addingCapacity(elements.length + 1);
        System.arraycopy(elements, index, elements, index + 1, this.size());
        elements[index] = element;
    }

    @Override
    public Planes remove(int index) {
        Planes removedElement = elements[index];
        System.arraycopy(elements, index + 1, elements, index, 1);
        elements[size()] = null;
        return removedElement;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < elements.length; i++)
            if (o.equals(elements[i]))
                return i;
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for(int i = elements.length - 1; i >= 0; i--)
            if (o.equals(elements[i]))
                return i;
        return -1;
    }

    @Override
    public ListIterator<Planes> listIterator() {
        return new PlaneListIterator(elements, counter);
    }

    @Override
    public ListIterator<Planes> listIterator(int index) {
        Planes[] notAllElements = new Planes[size() - index];
        System.arraycopy(elements, index, notAllElements, 0, size() - index);
        return new PlaneListIterator(notAllElements, counter);
    }

    @Override
    public List<Planes> subList(int fromIndex, int toIndex) {
        Planes[] subElements = new Planes[toIndex - fromIndex];
        for (int i = fromIndex; i < toIndex; i++)
            System.arraycopy(elements, fromIndex, subElements, 0, toIndex - fromIndex);
        List <Planes> SubPlanesList = Arrays.asList(subElements);
        return SubPlanesList;
    }

    public boolean addingCapacity(int checkLength){
        int oldLength = elements.length;
        if (checkLength > oldLength){
            int newLength = oldLength + (int)(oldLength * 0.3);
            if (newLength < checkLength)
                newLength = checkLength;
            elements = Arrays.copyOf(elements, newLength);
            return true;
        }
        return false;
    }
}
