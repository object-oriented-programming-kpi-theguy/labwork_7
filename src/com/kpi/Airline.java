package com.kpi;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by Yuriy on 08.05.2016.
 */
public class Airline {
    private Planes[] airline;

    //Setter
    public Planes[] SetAirline(Planes[] air){
        for (int i = 0; i < air.length; i++) {
            airline[i] = air[i];
        }
        return airline;
    }

    //Constructor
    public Airline(Planes[] air){
        airline = new Planes[air.length];
        SetAirline(air);
    }

    //All Planes and capacity
    public int GetCapacity (){
        int GetPlaneCapacity = 0;
        for (int i = 0; i < airline.length; i++){
            GetPlaneCapacity += airline[i].GetCapacity();
        }
        return GetPlaneCapacity;
    }

    public void SortAirline(){
        Comparator<Planes> Line = new RangeComparator();
        Arrays.sort(airline, Line);
    }

    public String[] FindConsumption(double m, double n){
        String[] find = new String[airline.length];
        int k = 0;
        for (int i = 0; i < airline.length; i++){
            if (airline[i].GetConsumption() >= m && airline[i].GetConsumption() <= n){
                find[k++] = airline[i].GetPlaneName();
            }
        }
        return find;
    }

    //Getter
    public String GetAirline(){
        String result = "";
        for (int i = 0; i < airline.length; i++){
            result = result + '\n' + airline[i].GetPlaneName()+" "+airline[i].GetRange()+" "+airline[i].GetConsumption()+" "+airline[i].GetCapacity();
        }
        return result;
    }
}
